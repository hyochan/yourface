package com.duchan.mobile.yourface.main.util

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.provider.MediaStore.Images
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.widget.Toast
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import android.util.Log
import java.io.FileNotFoundException


/**
 * Created by Busy on 2018. 9. 13..
 */

const val REQUEST_TAKE_PHOTO = 1
const val REQUEST_TAKE_ALBUM = 2
const val REQUEST_IMAGE_CROP = 3
const val REQUEST_CAMERA_PERMISSION = 0
const val REQUEST_STORAGE_PERMISSION = 1
private var photoFile: File? = null
private lateinit var mCurrentPhotoPath: String
private const val IMAGE_DIRECTORY_NAME = "YourFace"


fun showCamera(activity: Activity) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        captureImage(activity)
    } else {
        captureImage2(activity)
    }
}

fun showAlbum(activity: Activity) {
    if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
        ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_STORAGE_PERMISSION)
    } else {
        val intent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.type = "image/*"
        activity.startActivityForResult(intent, REQUEST_TAKE_ALBUM)
    }
}

fun doCrop(activity: Activity) {
    try {
        val cropIntent = Intent("com.android.camera.action.CROP")

        cropIntent.setDataAndType(Uri.fromFile(photoFile), "image/*")
        cropIntent.putExtra("crop", "true")
        cropIntent.putExtra("aspectX", 1)
        cropIntent.putExtra("aspectY", 1)
        cropIntent.putExtra("outputX", 128)
        cropIntent.putExtra("outputY", 128)
        cropIntent.putExtra("return-data", true)
        activity.startActivityForResult(cropIntent, REQUEST_IMAGE_CROP)
    } catch (e: ActivityNotFoundException) {
        Log.e("doCrop", e.localizedMessage)
    }
}

fun getCaptureBitmap(): Bitmap? {
    return if (photoFile != null) {
        BitmapFactory.decodeFile(photoFile!!.absolutePath)
    } else {
        null
    }
}

fun release() {
    photoFile = null
}

private fun captureImage2(activity: Activity) {
    try {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        photoFile = createImageFile2(activity)
        if (photoFile != null) {
            displayMessage(activity, photoFile!!.absolutePath)
            val photoURI = Uri.fromFile(photoFile)
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
            activity.startActivityForResult(cameraIntent, REQUEST_TAKE_PHOTO)
        }
    } catch (e: Exception) {
        displayMessage(activity, "Camera is not available." + e.toString())
    }
}

private fun captureImage(activity: Activity) {
    if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
        ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_CAMERA_PERMISSION)
    } else {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(activity.packageManager) != null) {
            try {
                photoFile = createImageFile(activity)
                if (photoFile != null) {
                    val photoURI = FileProvider.getUriForFile(activity,
                            "com.duchan.mobile.yourface.fileprovider",
                            photoFile!!)

                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)

                    activity.startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
                }
            } catch (ex: Exception) {
                displayMessage(activity, "Capture Image Bug: " + ex.message.toString())
            }
        } else {
            displayMessage(activity, "Null")
        }
    }
}

private fun createImageFile2(activity: Activity): File? {
    // External sdcard location
    val mediaStorageDir = File(
            Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
            IMAGE_DIRECTORY_NAME)
    // Create the storage directory if it does not exist
    if (!mediaStorageDir.exists()) {
        if (!mediaStorageDir.mkdirs()) {
            displayMessage(activity, "Unable to create directory.")
            return null
        }
    }

    val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss",
            Locale.getDefault()).format(Date())

    return File(mediaStorageDir.path + File.separator
            + "IMG_" + timeStamp + ".jpg")

}

@SuppressLint("SimpleDateFormat")
@Throws(IOException::class)
private fun createImageFile(activity: Activity): File {
    // Create an image file name
    val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
    val imageFileName = "JPEG_" + timeStamp + "_"
    val storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
    val image = File.createTempFile(
            imageFileName, /* prefix */
            ".jpg", /* suffix */
            storageDir      /* directory */
    )

    // Save a file: path for use with ACTION_VIEW intents
    mCurrentPhotoPath = image.absolutePath
    return image
}

private fun displayMessage(context: Context, message: String) {
    Toast.makeText(context, message, Toast.LENGTH_LONG).show()
}

fun insertImage(cr: ContentResolver,
                source: Bitmap?,
                title: String,
                description: String): String? {

    val values = ContentValues()
    values.put(Images.Media.TITLE, title)
    values.put(Images.Media.DISPLAY_NAME, title)
    values.put(Images.Media.DESCRIPTION, description)
    values.put(Images.Media.MIME_TYPE, "image/jpeg")
    // Add the date meta data to ensure the image is added at the front of the gallery
    values.put(Images.Media.DATE_ADDED, System.currentTimeMillis())
    values.put(Images.Media.DATE_TAKEN, System.currentTimeMillis())

    var url: Uri? = null
    var stringUrl: String? = null    /* value to be returned */

    try {
        url = cr.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)

        if (source != null) {
            val imageOut = cr.openOutputStream(url!!)
            try {
                source.compress(Bitmap.CompressFormat.JPEG, 80, imageOut)
            } finally {
                imageOut!!.close()
            }

            val id = ContentUris.parseId(url)
            // Wait until MINI_KIND thumbnail is generated.
            val miniThumb = MediaStore.Images.Thumbnails.getThumbnail(cr, id, Images.Thumbnails.MINI_KIND, null)
            // This is for backward compatibility.
            storeThumbnail(cr, miniThumb, id, 50f, 50f, Images.Thumbnails.MICRO_KIND)
        } else {
            cr.delete(url!!, null, null)
            url = null
        }
    } catch (e: Exception) {
        if (url != null) {
            cr.delete(url, null, null)
            url = null
        }
    }

    if (url != null) {
        val projection = Array(1) { MediaStore.MediaColumns.DATA }


        val metaCursor = cr.query(url, projection, null, null, null)
        if (metaCursor != null) {
            try {
                if (metaCursor.moveToFirst()) {
                    stringUrl = metaCursor.getString(0)
                }
            } finally {
                metaCursor.close()
            }
        }
    }

    return stringUrl
}

/**
 * A copy of the Android internals StoreThumbnail method, it used with the insertImage to
 * populate the android.provider.MediaStore.Images.Media#insertImage with all the correct
 * meta data. The StoreThumbnail method is private so it must be duplicated here.
 * @see android.provider.MediaStore.Images.Media
 */
private fun storeThumbnail(
        cr: ContentResolver,
        source: Bitmap,
        id: Long,
        width: Float,
        height: Float,
        kind: Int): Bitmap? {

    // create the matrix to scale it
    val matrix = Matrix()

    val scaleX = width / source.width
    val scaleY = height / source.height

    matrix.setScale(scaleX, scaleY)

    val thumb = Bitmap.createBitmap(source, 0, 0,
            source.width,
            source.height, matrix,
            true
    )

    val values = ContentValues(4)
    values.put(Images.Thumbnails.KIND, kind)
    values.put(Images.Thumbnails.IMAGE_ID, id.toInt())
    values.put(Images.Thumbnails.HEIGHT, thumb.height)
    values.put(Images.Thumbnails.WIDTH, thumb.width)

    val url = cr.insert(Images.Thumbnails.EXTERNAL_CONTENT_URI, values)

    try {
        val thumbOut = cr.openOutputStream(url!!)
        thumb.compress(Bitmap.CompressFormat.JPEG, 100, thumbOut)
        thumbOut!!.close()
        return thumb
    } catch (ex: FileNotFoundException) {
        return null
    } catch (ex: IOException) {
        return null
    }

}
