package com.duchan.mobile.yourface.common.uploader;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Pair;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author syuan
 * @since 2016-04-27
 */
public abstract class Uploader implements Runnable {

	protected static final Charset US_ASCII = Charset.forName("US-ASCII");
	protected static final Charset UTF8 = Charset.forName("UTF-8");

	protected Charset charset = UTF8;
	protected HttpConnector connector;
	protected String method;
	protected String url;
	protected String tag;
	protected boolean isFixedLengthStreamingMode;
	protected boolean isCancelled;
	protected OnUploadListener uploadListener;
	protected List<Pair<String, String>> headers = new ArrayList<>();
	protected List<Pair<String, String>> parameters = new ArrayList<>();
	protected List<Pair<String, File>> files = new ArrayList<>();

	private static Handler handler = new Handler(Looper.getMainLooper());

	public Uploader(String method, String url) {
		this.method = method;
		this.url = url;
	}

	@Override
	public void run() {
		try {
			onSetup();
			onUpload();
		} catch (Exception e) {
			invokeError(e);
		}

		if (isCancelled) {
			invokeCancel();
		}
	}

	public abstract void onSetup() throws UnsupportedEncodingException;

	public abstract void onUpload() throws Exception;

	public void start(){
		UploaderQueue.getInstance().enqueue(this);
	}

	public void cancel() {
		isCancelled = true;
	}

	public String getKey() {
		if (TextUtils.isEmpty(tag)) {
			tag = UUID.randomUUID().toString();
		}
		return tag;
	}

	public void addHeader(String key, String value) {
		headers.add(new Pair<>(key, value));
	}

	public void addParameter(String key, String value) {
		parameters.add(new Pair<>(key, value));
	}

	public void addFile(String name, String path) {
		if (path == null || "".equals(path)) {
			return;
		}

		File file = new File(path);
		if (!file.exists() || file.isDirectory()) {
			return;
		}
		files.add(new Pair<>(name, file));
	}

	public void addFile(String name, File file) {
		if (file == null) {
			return;
		}

		if (!file.exists() || file.isDirectory()) {
			return;
		}
		files.add(new Pair<>(name, file));
	}

	public void setFixedLengthStreamingMode(boolean isFixedLengthStreamingMode) {
		this.isFixedLengthStreamingMode = isFixedLengthStreamingMode;
	}

	public void setOnUploadListener(OnUploadListener uploadListener) {
		this.uploadListener = uploadListener;
	}

	protected void invokeProgress(final long progress, final long total) {
		if (uploadListener != null) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					uploadListener.onProgress(progress, total);
				}
			});
		}
	}

	protected void invokeError(final Exception e) {
		if (uploadListener != null) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					uploadListener.onError(e);
				}
			});
		}
	}

	protected void invokeResponse(final byte[] data) {
		if (uploadListener != null) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					uploadListener.onResponse(data);
				}
			});
		}
	}

	protected void invokeCancel(){
		if (uploadListener != null) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					uploadListener.onCancel();
				}
			});
		}
	}

	public interface OnUploadListener {

		void onProgress(long progress, long total);

		void onError(Exception e);

		void onResponse(byte[] data);

		void onCancel();
	}
}
