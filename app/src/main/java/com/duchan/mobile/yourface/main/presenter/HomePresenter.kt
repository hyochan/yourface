package com.duchan.mobile.yourface.main.presenter

import com.duchan.mobile.yourface.common.mvp.BaseMvpPresenter
import com.duchan.mobile.yourface.main.model.HomeModel
import com.duchan.mobile.yourface.main.view.HomeView

class HomePresenter : BaseMvpPresenter<HomeView, HomeModel>() {

    override fun createModel(): HomeModel {
        return HomeModel()
    }
}