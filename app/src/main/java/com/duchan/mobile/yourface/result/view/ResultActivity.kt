package com.duchan.mobile.yourface.result.view

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import com.duchan.mobile.yourface.R
import com.duchan.mobile.yourface.common.CommonActivity
import com.duchan.mobile.yourface.result.dto.CelebrityInfoVO
import com.duchan.mobile.yourface.result.dto.FaceInfoVO
import com.duchan.mobile.yourface.result.interactor.ImageUploadInteractor
import com.duchan.mobile.yourface.result.presenter.ResultPresenter
import kotlinx.android.synthetic.main.activity_result.*
import android.widget.TextView
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.AbsoluteSizeSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.Gravity
import android.view.ViewGroup
import com.duchan.mobile.yourface.common.util.DeviceUtil
import com.duchan.mobile.yourface.common.util.DeviceUtil.captureImage
import com.duchan.mobile.yourface.result.dto.ValueVO
import com.duchan.mobile.yourface.result.interactor.KakaoInteractor
import com.duchan.mobile.yourface.result.widget.CelebrityView
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import java.io.File
import java.io.IOException
import java.util.*


/**
 * Created by Busy on 2018. 9. 18..
 */
class ResultActivity : CommonActivity<Resultview, ResultPresenter>(), Resultview {

    private lateinit var mInterstitialAd: InterstitialAd

    override fun initLayoutView(): Int {
        return R.layout.activity_result
    }

    override fun createPresenter(): ResultPresenter {
        return ResultPresenter(ImageUploadInteractor(),
                KakaoInteractor(this))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (intent != null) {
            getPresenter().initProcess(intent)
        }

        mInterstitialAd = InterstitialAd(this)
        mInterstitialAd.adUnitId = getString(R.string.ad_mob_full_id)
        mInterstitialAd.loadAd(AdRequest.Builder().build())

        profile_layout.layoutParams.height = (DeviceUtil.getDeviceHeight(this) * 0.55f).toInt()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent != null) {
            getPresenter().initProcess(intent)
        }
    }

    override fun showProgress() {
        result_layout.visibility = View.GONE
        progress.visibility = View.VISIBLE
        empty_icon_img.setImageResource(R.mipmap.ic_face_scan)
        empty_desc.text = getString(R.string.loading)
        empty_layout.visibility = View.VISIBLE
        empty_layout.setOnClickListener(null)
    }

    override fun hideProgress() {
        progress.visibility = View.GONE
        empty_layout.visibility = View.GONE
    }

    override fun showMessage(msg: String) {
        val toast = Toast.makeText(this, msg + "\n" + getString(R.string.retry), Toast.LENGTH_SHORT)
        val view = toast.view.findViewById<TextView>(android.R.id.message)
        view?.let {
            view.gravity = Gravity.CENTER
        }
        toast.show()
    }

    override fun setImage(uri: Uri) {
        profile.setImageURI(uri)
    }

    override fun setSummaryResult(summaryResult: String) {
        profile_desc.text = summaryResult
    }

    override fun setMatchFaceData(faceInfoVO: FaceInfoVO) {
        empty_layout.visibility = View.GONE
        info_layout.visibility = View.VISIBLE
        result_layout.visibility = View.VISIBLE
        val emotionPercent = faceInfoVO.emotion!!.confidence
        val genderPercent = faceInfoVO.gender!!.confidence

        setEstimateText(age, faceInfoVO.age!!)
        smile_view.setPercent(emotionPercent)

        Log.d("hyo", "faceInfoVO.gender.value:" + faceInfoVO.gender.value)
        Log.d("hyo", "genderPercent:$genderPercent")

        if ("male" == faceInfoVO.gender.value) {
            male_view.setPercent(genderPercent)
            female_view.setPercent(1 - genderPercent)
        } else {
            female_view.setPercent(genderPercent)
            male_view.setPercent(1 - genderPercent)
        }

        share_btn.setOnClickListener {
            captureShareImage()
        }

        save_btn.setOnClickListener {
            captureSaveImage()
        }
    }

    override fun setMatchCelebrityData(celebrityList: List<CelebrityInfoVO>) {
        if (celebrityList.isNotEmpty()) {
            setEstimateText(celebrity, celebrityList[0].celebrity!!)
            celebrity.setOnClickListener {
                searchKeyword(celebrityList[0].celebrity!!.value!!)
            }

            if (celebrityList.size > 1) {
                for (i in 1 until celebrityList.size) {
                    flowLayout.addView(createCelebrityView(celebrityList[i]))
                }
                celebrity_etc_title.visibility = View.VISIBLE
                flowLayout.visibility = View.VISIBLE
            } else {
                celebrity_etc_title.visibility = View.GONE
                flowLayout.visibility = View.GONE
            }
        } else {
            celebrity_etc_title.visibility = View.GONE
            flowLayout.visibility = View.GONE
        }
    }

    private fun createCelebrityView(celebrityVO: CelebrityInfoVO): CelebrityView {
        val detractorOptionView = CelebrityView(this)
        detractorOptionView.layoutParams = ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        detractorOptionView.setData(celebrityVO.celebrity!!)
        detractorOptionView.setCelebrityListener(object : CelebrityView.OnClickCelebrityListener{
            override fun onClick(value: String) {
                searchKeyword(celebrityVO.celebrity.value!!)
            }

        })
        return detractorOptionView
    }

    private fun searchKeyword(keyword : String) {
        val uri = Uri.parse("https://m.search.naver.com/search.naver?query=$keyword")
        val intent = Intent(Intent.ACTION_VIEW, uri)
        startActivity(intent)
    }

    private fun setEstimateText(textView: TextView, valueVO: ValueVO) {
        val estimateText = SpannableStringBuilder(valueVO.value)
        estimateText.setSpan(StyleSpan(Typeface.BOLD), 0, valueVO.value!!.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        estimateText.setSpan(AbsoluteSizeSpan(18, true), 0, valueVO.value.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        estimateText.append("(")
        if (valueVO.confidence == 0f || valueVO.confidence == 1f) {
            estimateText.append(String.format("%.0f", valueVO.confidence * 100))
        } else {
            estimateText.append(String.format("%.1f", valueVO.confidence * 100))
        }

        estimateText.append("%)")
        estimateText.setSpan(StyleSpan(Typeface.NORMAL), valueVO.value.length, estimateText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        estimateText.setSpan(AbsoluteSizeSpan(14, true), valueVO.value.length, estimateText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        textView.text = estimateText
    }

    override fun setNotMatch() {
        empty_icon_img.setImageResource(R.drawable.ic_not_facial_recognition)
        empty_desc.text = getString(R.string.empty)
        result_layout.visibility = View.GONE
        info_layout.visibility = View.GONE
        empty_layout.visibility = View.VISIBLE
        empty_layout.setOnClickListener {
            CropImage.activity()
                    .setAutoZoomEnabled(true)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setRequestedSize(1024, 1024)
                    .start(this)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        try {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                val result = CropImage.getActivityResult(data)
                if (resultCode == Activity.RESULT_OK) {
                    val intent = Intent(this, ResultActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
                    intent.putExtra("resultData", result)
                    startActivity(intent)
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    showMessage("Cropping failed: " + result.error)
                }
            }
        } catch (e: Exception) {
            Log.e(javaClass.simpleName, e.localizedMessage)
        }
    }

    override fun showAd() {
        try {
            val handler = Handler()
            handler.postDelayed({
                if (mInterstitialAd.isLoaded) {
                    mInterstitialAd.show()
                }
            }, 2000)

        } catch (e: Exception) {
            Log.e(javaClass.simpleName, e.localizedMessage)
        }
    }

    private fun captureSaveImage() {
        content_layout.isDrawingCacheEnabled = true
        try {
            progress.visibility = View.VISIBLE
            captureImage(content_layout, "", object : DeviceUtil.deviceCallback {
                override fun success(file: File?) {
                    Toast.makeText(baseContext, "저장 되었습니다.", Toast.LENGTH_SHORT).show()
                    hideProgress()
                }

                override fun error(error: String) {
                    Log.e(javaClass.simpleName, error)
                    showMessage(error)
                    hideProgress()
                }
            })
        } catch (e: IOException) {
            Log.e(javaClass.simpleName, e.localizedMessage)
            showMessage(e.localizedMessage)
            hideProgress()
        }
    }

    private fun captureShareImage() {
        content_layout.isDrawingCacheEnabled = true
        try {
            progress.visibility = View.VISIBLE
            captureImage(content_layout, "capture", object : DeviceUtil.deviceCallback {
                override fun success(file: File?) {
                    getPresenter().getShareImage(file)
                }

                override fun error(error: String) {
                    Log.e(javaClass.simpleName, error)
                    showMessage(error)
                    hideProgress()
                }
            })
        } catch (e: IOException) {
            Log.e(javaClass.simpleName, e.localizedMessage)
            showMessage(e.localizedMessage)
            hideProgress()
        }
    }
}
