package com.duchan.mobile.yourface.result.dto

/**
 * Created by Busy on 2018. 9. 18..
 */
class RoiVO {
    val x: Int = 0
    val y: Int = 0
    val width: Int = 0
    val height: Int = 0

}