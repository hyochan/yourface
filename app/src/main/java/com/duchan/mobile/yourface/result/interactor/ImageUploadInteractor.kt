package com.duchan.mobile.yourface.result.interactor

import com.duchan.mobile.yourface.common.uploader.MultipartUploader
import com.duchan.mobile.yourface.common.uploader.Uploader
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

/**
 * Created by Busy on 2018. 9. 18..
 */
class ImageUploadInteractor @Throws(IOException::class)
constructor() {

    companion object {
        private const val URL = "https://your-face-hyochanj.c9users.io/upload"
    }

    fun requestFaceAnalyis(file: File, uploadListener: Uploader.OnUploadListener) {

        val uploader = MultipartUploader(URL)
        uploader.addFile("source", file)

        uploader.setFixedLengthStreamingMode(true)
        uploader.setOnUploadListener(uploadListener)
        uploader.start()
    }
}