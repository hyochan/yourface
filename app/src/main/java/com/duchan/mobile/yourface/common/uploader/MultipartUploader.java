package com.duchan.mobile.yourface.common.uploader;

import android.util.Pair;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Locale;

public class MultipartUploader extends Uploader {

	private static final String NEW_LINE = "\r\n";
	private static final String TWO_HYPHENS = "--";

	private long uploadedBytes;
	private long totalBytes;
	private byte[] boundaryBytes;
	private byte[] trailerBytes;

	public MultipartUploader(String url) {
		super("POST", url);
	}

	@Override
	public void onSetup() throws UnsupportedEncodingException {
		String boundary = "-------" + System.currentTimeMillis();
		addHeader("Connection", files.size() <= 1 ? "close" : "Keep-Alive");
		addHeader("Content-Type", "multipart/form-data; boundary=" + boundary);

		boundaryBytes = (NEW_LINE + TWO_HYPHENS + boundary + NEW_LINE).getBytes(US_ASCII);
		trailerBytes = (NEW_LINE + TWO_HYPHENS + boundary + TWO_HYPHENS + NEW_LINE).getBytes(US_ASCII);
	}

	@Override
	public void onUpload() throws Exception {
		try {
			totalBytes = getBodyLength();

			connector = HttpConnector.create(method, url);
			connector.setFixedLengthStreamingMode(isFixedLengthStreamingMode, totalBytes);
			connector.setHeader(headers);
			writeBody(connector);

			if (!isCancelled) {
				boolean isSucceed = ((connector.getResponseCode() / 100) == 2);
				if (isSucceed) {
					invokeResponse(connector.getResponseBody());
				} else {
					invokeError(new IOException("Server Error"));
				}
			}
		} finally {
			if (connector != null) {
				connector.close();
			}
		}
	}

	private long getBodyLength() throws UnsupportedEncodingException {
		// calculate parameter length
		long parameterBytes = 0;
		for (Pair<String, String> parameter : parameters) {
			parameterBytes += boundaryBytes.length + getMultipartParameterBytes(parameter, charset).length;
		}

		// calculate file length
		long fileBytes = 0;
		for (Pair<String, File> file : files) {
			fileBytes += (boundaryBytes.length + getMultipartFileHeaderBytes(file, charset).length + file.second.length());
		}

		return (parameterBytes + fileBytes + trailerBytes.length);
	}

	private void writeBody(HttpConnector connector) throws IOException {
		// write parameters
		for (Pair<String, String> parameter : parameters) {
			connector.writeBody(boundaryBytes);
			byte[] formItemBytes = getMultipartParameterBytes(parameter, charset);
			connector.writeBody(formItemBytes);

			uploadedBytes += boundaryBytes.length + formItemBytes.length;
			invokeProgress(uploadedBytes, totalBytes);
		}

		// write files
		for (Pair<String, File> file : files) {
			if (isCancelled) {
				break;
			}
			connector.writeBody(boundaryBytes);
			byte[] headerBytes = getMultipartFileHeaderBytes(file, charset);
			connector.writeBody(headerBytes);

			uploadedBytes += boundaryBytes.length + headerBytes.length;
			invokeProgress(uploadedBytes, totalBytes);
			writeFile(file.second);
		}

		connector.writeBody(trailerBytes);
	}

	private void writeFile(File file) throws FileNotFoundException {
		InputStream stream = new FileInputStream(file);
		try {
			byte[] buffer = new byte[2048];
			int bytesRead;
			while ((bytesRead = stream.read(buffer, 0, buffer.length)) > 0 && !isCancelled) {
				connector.writeBody(buffer, bytesRead);
				uploadedBytes += bytesRead;
				invokeProgress(uploadedBytes, totalBytes);
			}
		} catch (IOException e) {
			// do nothing
		} finally {
			try {
				stream.close();
			} catch (Exception e) {
				// do nothing
			}
		}
	}

	private static byte[] getMultipartParameterBytes(Pair<String, String> pair, Charset charset) throws UnsupportedEncodingException {
		return ("Content-Disposition: form-data; name=\"" + pair.first + "\"" + NEW_LINE + NEW_LINE + pair.second).getBytes(charset);
	}

	private byte[] getMultipartFileHeaderBytes(Pair<String, File> file, Charset charset) throws UnsupportedEncodingException {
		StringBuilder builder = new StringBuilder();
		builder.append("Content-Disposition: form-data; name=\"")
				.append(file.first).append("\"; filename=\"")
				.append(file.second.getName()).append("\"").append(NEW_LINE);
		builder.append("Content-Type: ").append(detectMimeType(file.second)).append(NEW_LINE).append(NEW_LINE);
		return builder.toString().getBytes(charset);
	}

	private static String detectMimeType(File file) {
		String extension = null;
		String absolutePath = file.getAbsolutePath();
		int index = absolutePath.lastIndexOf('.') + 1;
		if (index >= 0 && index <= absolutePath.length()) {
			extension = absolutePath.substring(index);
		}

		if (extension == null || extension.isEmpty()) {
			return "octet-stream";
		}

		String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.toLowerCase(Locale.getDefault()));
		if (mimeType == null) {
			return "octet-stream";
		}
		return mimeType;
	}
}
