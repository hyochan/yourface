package com.duchan.mobile.yourface.result.dto

/**
 * Created by Busy on 2018. 9. 18..
 */
class ValueVO {
    val value: String? = null
    var confidence: Float = 0.0f
}