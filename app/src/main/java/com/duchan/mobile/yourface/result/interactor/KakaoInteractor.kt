package com.duchan.mobile.yourface.result.interactor

import android.content.Context
import android.widget.Toast
import com.duchan.mobile.yourface.R
import com.kakao.kakaolink.v2.KakaoLinkResponse
import com.kakao.kakaolink.v2.KakaoLinkService
import com.kakao.kakaolink.v2.model.ContentObject
import com.kakao.kakaolink.v2.model.FeedTemplate
import com.kakao.kakaolink.v2.model.LinkObject
import com.kakao.network.ErrorResult
import com.kakao.network.callback.ResponseCallback
import com.kakao.network.storage.ImageUploadResponse
import java.io.File

/**
 * Created by Busy on 2018. 9. 21..
 */
class KakaoInteractor(private val context: Context) {

    interface Callback {
        fun onFailure(errorResult: ErrorResult)
        fun onSuccess(result: KakaoLinkResponse)
    }

    fun shareImage(file: File?, callback: Callback) {
        if (file != null) {
            KakaoLinkService.getInstance().uploadImage(context, false, file, object : ResponseCallback<ImageUploadResponse>() {
                override fun onFailure(errorResult: ErrorResult) {
                    callback.onFailure(errorResult)
                }

                override fun onSuccess(result: ImageUploadResponse) {
                    sendShare(result, callback)
                }
            })
        }
    }

    private fun sendShare(result: ImageUploadResponse, callback: Callback) {
        val schemeUri = context.getString(R.string.kakao_scheme) + "://" + context.getString(R.string.kakaolink_host)
        val linkObject = LinkObject.newBuilder().setWebUrl(schemeUri)
                .setMobileWebUrl(schemeUri)

        val params = FeedTemplate
                .newBuilder(ContentObject.newBuilder(context.getString(R.string.app_name),
                        result.original.url, linkObject.build())
                        .setDescrption(context.getString(R.string.kakao_desc))
                        .setImageWidth(result.original.width)
                        .setImageHeight(result.original.height)
                        .build())
                .build()

        KakaoLinkService.getInstance().sendDefault(context, params, object : ResponseCallback<KakaoLinkResponse>() {
            override fun onFailure(errorResult: ErrorResult) {
                callback.onFailure(errorResult)
            }

            override fun onSuccess(result: KakaoLinkResponse) {
                callback.onSuccess(result)
            }
        })

    }
}