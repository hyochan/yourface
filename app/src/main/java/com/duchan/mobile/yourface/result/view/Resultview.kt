package com.duchan.mobile.yourface.result.view

import android.net.Uri
import com.duchan.mobile.yourface.common.mvp.MvpView
import com.duchan.mobile.yourface.result.dto.CelebrityInfoVO
import com.duchan.mobile.yourface.result.dto.FaceInfoVO

/**
 * Created by Busy on 2018. 9. 18..
 */
interface Resultview : MvpView {

    fun showProgress()
    fun hideProgress()
    fun showMessage(msg: String)
    fun setImage(uri: Uri)
    fun setSummaryResult(summaryResult:String)
    fun setMatchFaceData(faceInfoVO: FaceInfoVO)
    fun setMatchCelebrityData(celebrityList: List<CelebrityInfoVO>)
    fun setNotMatch()
    fun showAd()
    fun finish()
}