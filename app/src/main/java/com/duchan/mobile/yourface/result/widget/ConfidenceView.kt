package com.duchan.mobile.yourface.result.widget

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.duchan.mobile.yourface.R
import kotlinx.android.synthetic.main.view_confidence.view.*

/**
 * Created by Busy on 2018. 9. 17..
 */
class ConfidenceView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {
    init {
        LayoutInflater.from(context).inflate(R.layout.view_confidence, this, true)

        attrs?.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.Confident)
            val iconImg = typedArray.getResourceId(R.styleable.Confident_icon, -1)
            val percent = typedArray.getFloat(R.styleable.Confident_percent, 0.0f)

            confidence_img.setImageResource(iconImg)
            setPercent(percent)

            typedArray.recycle()
        }
    }

    fun setPercent(percent: Float) {
        setPercent(percent, true)
    }

    @SuppressLint("SetTextI18n")
    fun setPercent(percent: Float, showText: Boolean) {
        (confidence_progress.layoutParams as LinearLayout.LayoutParams).weight = percent
        (confidence_background.layoutParams as LinearLayout.LayoutParams).weight = 1 - percent

        val percentText = percent * 100
        if (percentText == 0f || percentText == 100f) {
            percent_text.text = String.format("%.0f", percentText) + "%"
        } else {
            percent_text.text = String.format("%.1f", percentText) + "%"
        }

        if (showText) {
            percent_text.visibility = View.VISIBLE
        } else {
            percent_text.visibility = View.GONE
        }
        requestLayout()
    }

}
