package com.duchan.mobile.yourface.main.presenter

import com.duchan.mobile.yourface.common.mvp.BaseMvpPresenter
import com.duchan.mobile.yourface.main.model.MainModel
import com.duchan.mobile.yourface.main.view.MainView

class MainPresenter : BaseMvpPresenter<MainView, MainModel>() {

    override fun createModel(): MainModel {
        return MainModel()
    }
}