package com.duchan.mobile.yourface.result.widget

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import android.util.TypedValue
import com.duchan.mobile.yourface.R
import com.duchan.mobile.yourface.result.dto.ValueVO

/**
 * Created by Busy on 2018. 9. 19..
 */
class CelebrityView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : AppCompatTextView(context, attrs, defStyleAttr) {
    private var celebrityListener: OnClickCelebrityListener? = null

    interface OnClickCelebrityListener {
        fun onClick(value: String)
    }

    init {
        initView(context)
    }

    private fun initView(context: Context) {
        val paddingTopBottom = dipToPixel(8)
        val paddingLeftRight = dipToPixel(17)
        setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight, paddingTopBottom)
        setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14f)
        setBackgroundResource(R.drawable.bg_white_with_eee_stroke_round)
        setTextColor(ContextCompat.getColor(context, R.color.black_555555))
    }

    fun setData(valueVO: ValueVO) {
        tag = valueVO
        text = valueVO.value!!
        setOnClickListener {
            if (celebrityListener != null) {
                celebrityListener!!.onClick(valueVO.value)
            }
        }
    }

    fun setCelebrityListener(celebrityListener: OnClickCelebrityListener) {
        this.celebrityListener = celebrityListener
    }

    fun dipToPixel(dip: Int): Int {
        return try {
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip.toFloat(), resources.displayMetrics).toInt()
        } catch (e: Exception) {
            dip
        }

    }
}
