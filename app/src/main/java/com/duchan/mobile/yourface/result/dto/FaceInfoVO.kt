package com.duchan.mobile.yourface.result.dto

/**
 * Created by Busy on 2018. 9. 18..
 */
class FaceInfoVO {
    val roi: RoiVO? = null
    val gender: ValueVO? = null
    val age: ValueVO? = null
    val emotion: ValueVO? = null
    val pose: ValueVO? = null
}