package com.duchan.mobile.yourface.result.dto

/**
 * Created by Busy on 2018. 9. 18..
 */
class ResultVO {
    val face: FaceVO? = null
    val celebrity: CelebrityVO? = null
}