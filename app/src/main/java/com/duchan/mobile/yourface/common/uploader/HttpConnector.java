package com.duchan.mobile.yourface.common.uploader;

import android.annotation.TargetApi;
import android.os.Build;
import android.util.Pair;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class HttpConnector {

	private static final int BUFFER_SIZE = 2048;
	private static final int CONNECTION_TIMEOUT = 5000;
	private static final int READ_TIMEOUT = 5000;

	private HttpURLConnection connection;

	public static HttpConnector create(String method, String url) throws IOException {
		return new HttpConnector(method, url, CONNECTION_TIMEOUT, READ_TIMEOUT);
	}

	private HttpConnector(String method, String url, int connectTimeout, int readTimeout) throws IOException {
		connection = (HttpURLConnection) new URL(url).openConnection();
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setConnectTimeout(connectTimeout);
		connection.setReadTimeout(readTimeout);
		connection.setUseCaches(false);
		connection.setInstanceFollowRedirects(false);
		connection.setRequestMethod(method);
	}

	@TargetApi(Build.VERSION_CODES.KITKAT)
	public void setFixedLengthStreamingMode(boolean isFixedLengthStreamingMode, long totalBodyBytes) {
		if (isFixedLengthStreamingMode) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
				connection.setFixedLengthStreamingMode(totalBodyBytes);
			} else {
				if (totalBodyBytes > Integer.MAX_VALUE) {
					throw new RuntimeException("You need Android API version 19 or newer to "
						+ "upload more than 2GB in a single request using "
						+ "fixed size content length. Try switching to "
						+ "chunked mode instead, but make sure your server side supports it!");
				}
				connection.setFixedLengthStreamingMode((int) totalBodyBytes);
			}
		} else {
			connection.setChunkedStreamingMode(0);
		}
	}

	public void setHeader(List<Pair<String, String>> headers) {
		for (Pair<String, String> header : headers) {
			connection.setRequestProperty(header.first, header.second);
		}
	}

	public void writeBody(byte[] bytes) throws IOException {
		connection.getOutputStream().write(bytes, 0, bytes.length);
	}

	public void writeBody(byte[] bytes, int lengthToWriteFromStart) throws IOException {
		connection.getOutputStream().write(bytes, 0, lengthToWriteFromStart);
	}

	public int getResponseCode() throws IOException {
		return connection.getResponseCode();
	}

	public byte[] getResponseBody() throws IOException {
		InputStream stream = null;
		try {
			if (connection.getResponseCode() / 100 == 2) {
				stream = connection.getInputStream();
			} else {
				stream = connection.getErrorStream();
			}
			return getResponseBodyAsByteArray(stream);
		} finally {
			if (stream != null) {
				try {
					stream.close();
				} catch (Exception e) {
					// do nothing
				}
			}
		}
	}

	public void close() {
		if (connection != null) {
			try {
				connection.getInputStream().close();
			} catch (Exception ignored) {
			}

			try {
				connection.getOutputStream().flush();
				connection.getOutputStream().close();
			} catch (Exception ignored) {
			}

			try {
				connection.disconnect();
			} catch (Exception exc) {
				// // do nothing
			}
		}
	}

	private static byte[] getResponseBodyAsByteArray(final InputStream inputStream) {
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		byte[] buffer = new byte[BUFFER_SIZE];
		int bytesRead;
		try {
			while ((bytesRead = inputStream.read(buffer, 0, buffer.length)) > 0) {
				byteStream.write(buffer, 0, bytesRead);
			}
		} catch (Exception ignored) {
		}
		return byteStream.toByteArray();
	}
}
