package com.duchan.mobile.yourface.main.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.duchan.mobile.yourface.R
import com.duchan.mobile.yourface.common.CommonActivity
import com.duchan.mobile.yourface.common.util.FragmentHelper
import com.duchan.mobile.yourface.main.presenter.MainPresenter
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import android.util.Base64.NO_WRAP
import android.provider.SyncStateContract.Helpers.update
import android.content.pm.PackageManager
import com.google.android.gms.common.util.ClientLibraryUtils.getPackageInfo
import android.content.pm.PackageInfo
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*


class MainActivity : CommonActivity<MainView, MainPresenter>(), MainView {
    lateinit var mAdView : AdView
    private var fragmentHelper : FragmentHelper = FragmentHelper(this, R.id.content_body)
    private val fragment = HomeFragment()

    override fun initLayoutView(): Int {
        return R.layout.activity_main
    }

    override fun createPresenter(): MainPresenter {
        return MainPresenter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        MobileAds.initialize(this, getString(R.string.ad_mob_id))
        mAdView = findViewById(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.loadAd(adRequest)

        fragmentHelper.replaceFragment(fragment)
    }

    override fun onBackPressed() {
        if (fragmentHelper.hasRemovableFragment()) {
            fragmentHelper.removeFragment()
        } else {
            finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        fragment.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}
