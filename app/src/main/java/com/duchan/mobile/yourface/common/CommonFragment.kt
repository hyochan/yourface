package com.duchan.mobile.yourface.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.duchan.mobile.yourface.common.mvp.BaseMvpFragment
import com.duchan.mobile.yourface.common.mvp.MvpPresenter
import com.duchan.mobile.yourface.common.mvp.MvpView

abstract class CommonFragment<in V : MvpView, out P : MvpPresenter<V>> : BaseMvpFragment<V, P>() {

    protected abstract fun initLayoutView() : Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(initLayoutView(), container, false)
        return view
    }

    override fun attachView(view: V) {
    }

    override fun detachView() {
    }
}