package com.duchan.mobile.yourface.result.widget

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import com.duchan.mobile.yourface.R
import java.util.ArrayList

/**
 * Created by Busy on 2018. 9. 18..
 */
class FlowLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : ViewGroup(context, attrs) {
    var gravity = Gravity.START or Gravity.TOP
        set(gravity) {
            var gravity = gravity
            if (this.gravity != gravity) {
                if (gravity and Gravity.RELATIVE_HORIZONTAL_GRAVITY_MASK == 0) {
                    gravity = gravity or Gravity.START
                }

                if (gravity and Gravity.VERTICAL_GRAVITY_MASK == 0) {
                    gravity = gravity or Gravity.TOP
                }

                field = gravity
                requestLayout()
            }
        }
    private var mVerticalSpacing = 2
    private var mHorizontalSpacing = 10

    private val mLines = ArrayList<MutableList<View>>()
    private val mLineHeights = ArrayList<Int>()
    private val mLineMargins = ArrayList<Int>()

    init {
        val a = context.obtainStyledAttributes(attrs, R.styleable.FlowLayout)
        try {
            mHorizontalSpacing = a.getDimensionPixelSize(R.styleable.FlowLayout_horizontalSpace, 10)
            mVerticalSpacing = a.getDimensionPixelSize(R.styleable.FlowLayout_verticalSpace, 2)
            val index = a.getInt(R.styleable.FlowLayout_android_gravity, -1)
            if (index > 0) {
                gravity = index
            }
        } finally {
            a.recycle()
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val sizeWidth = View.MeasureSpec.getSize(widthMeasureSpec) - paddingLeft - paddingRight
        val sizeHeight = View.MeasureSpec.getSize(heightMeasureSpec)

        val modeWidth = View.MeasureSpec.getMode(widthMeasureSpec)
        val modeHeight = View.MeasureSpec.getMode(heightMeasureSpec)

        var width = 0
        var height = paddingTop + paddingBottom

        var lineWidth = 0
        var lineHeight = 0

        val childCount = childCount

        for (i in 0 until childCount) {
            val child = getChildAt(i)
            val lastChild = i == childCount - 1

            if (child.visibility == View.GONE) {
                if (lastChild) {
                    width = Math.max(width, lineWidth)
                    height += lineHeight
                }
                continue
            }

            measureChildWithMargins(child, widthMeasureSpec, lineWidth, heightMeasureSpec, height)

            val lp = child.layoutParams as LayoutParams
            var childWidthMode = View.MeasureSpec.AT_MOST
            var childWidthSize = sizeWidth

            var childHeightMode = View.MeasureSpec.AT_MOST
            var childHeightSize = sizeHeight

            if (lp.width == MarginLayoutParams.MATCH_PARENT) {
                childWidthMode = View.MeasureSpec.EXACTLY
                childWidthSize -= lp.leftMargin + lp.rightMargin
            } else if (lp.width >= 0) {
                childWidthMode = View.MeasureSpec.EXACTLY
                childWidthSize = lp.width
            }

            if (lp.height >= 0) {
                childHeightMode = View.MeasureSpec.EXACTLY
                childHeightSize = lp.height
            } else if (modeHeight == View.MeasureSpec.UNSPECIFIED) {
                childHeightMode = View.MeasureSpec.UNSPECIFIED
                childHeightSize = 0
            }

            child.measure(
                    View.MeasureSpec.makeMeasureSpec(childWidthSize, childWidthMode),
                    View.MeasureSpec.makeMeasureSpec(childHeightSize, childHeightMode)
            )

            val childWidth = child.measuredWidth + lp.leftMargin + lp.rightMargin + mHorizontalSpacing

            if (lineWidth + childWidth > sizeWidth) {
                width = Math.max(width, lineWidth)
                lineWidth = childWidth

                height += lineHeight
                lineHeight = child.measuredHeight + lp.topMargin + lp.bottomMargin + mVerticalSpacing
            } else {
                lineWidth += childWidth
                lineHeight = Math.max(lineHeight, child.measuredHeight + lp.topMargin + lp.bottomMargin)
            }

            if (lastChild) {
                width = Math.max(width, lineWidth)
                height += lineHeight
            }
        }

        width += paddingLeft + paddingRight

        setMeasuredDimension(
                if (modeWidth == View.MeasureSpec.EXACTLY) sizeWidth else width,
                if (modeHeight == View.MeasureSpec.EXACTLY) sizeHeight else height)
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        mLines.clear()
        mLineHeights.clear()
        mLineMargins.clear()

        val width = width
        val height = height

        var linesSum = paddingTop

        var lineWidth = 0
        var lineHeight = 0
        var lineViews: MutableList<View> = ArrayList()

        val horizontalGravityFactor: Float
        when (gravity and Gravity.HORIZONTAL_GRAVITY_MASK) {
            Gravity.LEFT -> horizontalGravityFactor = 0f
            Gravity.CENTER_HORIZONTAL -> horizontalGravityFactor = .5f
            Gravity.RIGHT -> horizontalGravityFactor = 1f
            else -> horizontalGravityFactor = 0f
        }

        for (i in 0 until childCount) {
            val child = getChildAt(i)
            if (child.visibility == View.GONE) {
                continue
            }

            val lp = child.layoutParams as LayoutParams

            val childWidth = child.measuredWidth + lp.leftMargin + lp.rightMargin + mHorizontalSpacing
            val childHeight = child.measuredHeight + lp.bottomMargin + lp.topMargin

            if (lineWidth + childWidth > width) {
                mLineHeights.add(lineHeight)
                mLines.add(lineViews)
                mLineMargins.add(((width - lineWidth) * horizontalGravityFactor).toInt() + paddingLeft)

                linesSum += lineHeight

                lineHeight = 0
                lineWidth = 0
                lineViews = ArrayList()
            }

            lineWidth += childWidth
            lineHeight = Math.max(lineHeight, childHeight)
            lineViews.add(child)
        }

        mLineHeights.add(lineHeight)
        mLines.add(lineViews)
        mLineMargins.add(((width - lineWidth) * horizontalGravityFactor).toInt() + paddingLeft)

        linesSum += lineHeight

        var verticalGravityMargin = 0
        when (gravity and Gravity.VERTICAL_GRAVITY_MASK) {
            Gravity.TOP -> {
            }
            Gravity.CENTER_VERTICAL -> verticalGravityMargin = (height - linesSum) / 2
            Gravity.BOTTOM -> verticalGravityMargin = height - linesSum
            else -> {
            }
        }

        val numLines = mLines.size

        var left: Int
        var top = paddingTop

        for (i in 0 until numLines) {
            lineHeight = mLineHeights[i]
            lineViews = mLines[i]
            left = mLineMargins[i]

            val children = lineViews.size

            for (j in 0 until children) {
                val child = lineViews[j]

                if (child.visibility == View.GONE) {
                    continue
                }

                val lp = child.layoutParams as LayoutParams

                if (lp.height == MarginLayoutParams.MATCH_PARENT) {
                    var childWidthMode = View.MeasureSpec.AT_MOST
                    var childWidthSize = lineWidth

                    if (lp.width == MarginLayoutParams.MATCH_PARENT) {
                        childWidthMode = View.MeasureSpec.EXACTLY
                    } else if (lp.width >= 0) {
                        childWidthMode = View.MeasureSpec.EXACTLY
                        childWidthSize = lp.width
                    }

                    child.measure(
                            View.MeasureSpec.makeMeasureSpec(childWidthSize, childWidthMode),
                            View.MeasureSpec.makeMeasureSpec(lineHeight - lp.topMargin - lp.bottomMargin, View.MeasureSpec.EXACTLY)
                    )
                }

                val childWidth = child.measuredWidth
                val childHeight = child.measuredHeight

                var gravityMargin = 0

                if (Gravity.isVertical(lp.gravity)) {
                    when (lp.gravity) {
                        Gravity.TOP -> {
                        }
                        Gravity.CENTER_VERTICAL, Gravity.CENTER -> gravityMargin = (lineHeight - childHeight - lp.topMargin - lp.bottomMargin) / 2
                        Gravity.BOTTOM -> gravityMargin = lineHeight - childHeight - lp.topMargin - lp.bottomMargin
                        else -> {
                        }
                    }
                }

                child.layout(left + lp.leftMargin,
                        top + lp.topMargin + gravityMargin + verticalGravityMargin,
                        left + childWidth + lp.leftMargin,
                        top + childHeight + lp.topMargin + gravityMargin + verticalGravityMargin)

                left += childWidth + lp.leftMargin + lp.rightMargin + mHorizontalSpacing
            }

            top += lineHeight + mVerticalSpacing
        }
    }

    override fun generateLayoutParams(p: ViewGroup.LayoutParams): LayoutParams {
        return LayoutParams(p)
    }

    override fun generateLayoutParams(attrs: AttributeSet): LayoutParams {
        return LayoutParams(context, attrs)
    }

    override fun generateDefaultLayoutParams(): LayoutParams {
        return LayoutParams(MarginLayoutParams.MATCH_PARENT, MarginLayoutParams.MATCH_PARENT)
    }

    override fun checkLayoutParams(p: ViewGroup.LayoutParams): Boolean {
        return super.checkLayoutParams(p) && p is LayoutParams
    }

    class LayoutParams : ViewGroup.MarginLayoutParams {

        var gravity = -1

        constructor(c: Context, attrs: AttributeSet) : super(c, attrs) {

            val a = c.obtainStyledAttributes(attrs, R.styleable.DetractorFlowLayout_Layout)

            try {
                gravity = a.getInt(R.styleable.DetractorFlowLayout_Layout_android_layout_gravity, -1)
            } finally {
                a.recycle()
            }
        }

        constructor(width: Int, height: Int) : super(width, height) {}

        constructor(source: ViewGroup.LayoutParams) : super(source) {}
    }
}