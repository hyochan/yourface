package com.duchan.mobile.yourface.common.mvp

interface MvpPresenter<in VIEW : MvpView> {

    /**
     * View Attach.
     */
    fun attachView(view: VIEW)

    /**
     * View detach
     */
    fun detachView()
}