package com.duchan.mobile.yourface.result.dto

/**
 * Created by Busy on 2018. 9. 18..
 */
class SizeVO {
    val width: String? = null
    val height: String? = null
}