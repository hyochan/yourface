package com.duchan.mobile.yourface.common.util

import com.google.gson.Gson
import java.lang.reflect.Type

/**
 * Created by Busy on 2018. 9. 18..
 */
object JsonUtil {

    fun <T> extractVoFromJson(jsonString: String, clazz: Class<T>): T {
        return Gson().fromJson(jsonString, clazz)
    }

    fun <T> extractVoFromJson(jsonString: String, type: Type): T {
        return Gson().fromJson(jsonString, type)
    }

    fun mapToJsonString(map: Map<String, String>): String {
        return Gson().toJson(map)
    }

}//do nothing
