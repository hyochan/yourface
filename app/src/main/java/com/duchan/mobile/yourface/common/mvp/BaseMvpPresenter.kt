package com.duchan.mobile.yourface.common.mvp

import android.support.annotation.NonNull
import android.support.annotation.UiThread
import java.lang.ref.WeakReference
import java.lang.reflect.ParameterizedType

abstract class BaseMvpPresenter<VIEW : MvpView, MODEL> : MvpPresenter<VIEW> {

    private var model: MODEL = createModel()

    private var view: WeakReference<VIEW>? = null
    private var nullView: VIEW? = null

    override fun attachView(view: VIEW) {
        this.view = WeakReference(view)
    }

    override fun detachView() {
        if (view != null) {
            view!!.clear()
            view = null
        }
    }

    @UiThread
    protected fun view(): VIEW? {
        if (view != null) {
            val realView = view!!.get()
            if (realView != null) {
                return realView
            }
        }

        return getNullView()
    }

    private fun getNullView(): VIEW? {
        if (nullView == null) {
            try {
                // Scan the inheritance hierarchy until we reached BasePresenter
                var viewClass: Class<VIEW>? = null
                var currentClass: Class<*> = javaClass

                while (viewClass == null) {
                    var genericSuperType = currentClass.genericSuperclass
                    while (genericSuperType !is ParameterizedType) {
                        // Scan inheritance tree until we find ParameterizedType which is probably a MvpSubclass
                        currentClass = currentClass.superclass
                        genericSuperType = currentClass.genericSuperclass
                    }

                    val types = (genericSuperType as ParameterizedType).actualTypeArguments
                    for (type in types) {
                        val genericType = type as Class<*>
                        if (genericType.isInterface && isSubTypeOfMvpView(genericType)) {

                            viewClass = genericType as Class<VIEW>
                            break
                        }
                    }

                    // Continue with next class in inheritance hierachy (see genericSuperType assignment at start of while loop)
                    currentClass = currentClass.superclass
                }
                nullView = NoOp.of(viewClass)
            } catch (t: Throwable) {    //NOSONAR Both exceptions and errors have to be caught.
                throw IllegalArgumentException(
                        "The generic type <V extends MvpView> must be the first generic type argument of class "
                                + javaClass.simpleName
                                + " (per convention). Otherwise we can't determine which type of View this"
                                + " Presenter coordinates.", t)
            }

        }
        return nullView
    }

    private fun isSubTypeOfMvpView(klass: Class<*>?): Boolean {
        if (klass == null) {
            return false
        }
        if (klass == MvpView::class.java) {
            return true
        }
        val superInterfaces = klass.interfaces
        for (i in superInterfaces.indices) {
            if (isSubTypeOfMvpView(superInterfaces[0])) {
                return true
            }
        }
        return false
    }

    fun setModel(model: MODEL) {
        this.model = model
    }

    @NonNull
    fun model(): MODEL {
        return model
    }

    protected abstract fun createModel(): MODEL
}