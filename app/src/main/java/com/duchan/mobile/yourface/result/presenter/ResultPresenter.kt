package com.duchan.mobile.yourface.result.presenter

import android.content.Intent
import android.support.annotation.NonNull
import android.util.Log
import com.duchan.mobile.yourface.common.mvp.BaseMvpPresenter
import com.duchan.mobile.yourface.common.uploader.Uploader
import com.duchan.mobile.yourface.common.util.JsonUtil
import com.duchan.mobile.yourface.result.dto.CelebrityInfoVO
import com.duchan.mobile.yourface.result.dto.FaceInfoVO
import com.duchan.mobile.yourface.result.interactor.ImageUploadInteractor
import com.duchan.mobile.yourface.result.dto.JsonResultVO
import com.duchan.mobile.yourface.result.interactor.KakaoInteractor
import com.duchan.mobile.yourface.result.model.ResultModel
import com.duchan.mobile.yourface.result.view.Resultview
import com.kakao.kakaolink.v2.KakaoLinkResponse
import com.kakao.network.ErrorResult
import com.theartofdev.edmodo.cropper.CropImage
import java.io.File
import java.lang.Exception


/**
 * Created by Busy on 2018. 9. 18..
 */
class ResultPresenter(private val imageUploadInteractor: ImageUploadInteractor, private val kakaoInteractor: KakaoInteractor) : BaseMvpPresenter<Resultview, ResultModel>()
        , Uploader.OnUploadListener, KakaoInteractor.Callback {

    private val CAPTURE_FILE_NAME = "capture.png"
    override fun createModel(): ResultModel {
        return ResultModel()
    }

    fun initProcess(@NonNull intent: Intent) {
        val result = intent.getParcelableExtra<CropImage.ActivityResult>("resultData")
        if (result != null) {
            val uri = result.uri

            if (uri != null) {
                view()?.setImage(uri)
                view()?.showProgress()
                requestFaceAnalysis(File(uri.path))
            } else {
                view()?.finish()
            }
        } else {
            view()?.finish()
        }
    }

    private fun requestFaceAnalysis(file: File) {
        imageUploadInteractor.requestFaceAnalyis(file, this)
    }

    override fun onProgress(progress: Long, total: Long) {
        //do not use
    }

    override fun onError(e: Exception?) {
        view()?.hideProgress()
        if (e != null) {
            view()?.showMessage(e.localizedMessage)
        }
        view()?.setNotMatch()
    }

    override fun onResponse(data: ByteArray?) {
        view()?.hideProgress()
        try {
            if (data == null) {
                return
            }

            val jsonResultVO = JsonUtil.extractVoFromJson(String(data), JsonResultVO::class.java)
            val resultVO = jsonResultVO.rData

            if (resultVO?.face?.info?.faceCount!! > 0) {
                view()?.setSummaryResult(getDescription(resultVO.face.faces!![0], resultVO.celebrity!!.faces!![0]))
                view()?.setMatchFaceData(resultVO.face.faces!![0])
                view()?.setMatchCelebrityData(resultVO.celebrity!!.faces!!)
            } else {
                view()?.setNotMatch()
            }
            view()?.showAd()
        } catch (e: Exception) {
            view()?.setNotMatch()
            view()?.showMessage(e.localizedMessage)
            Log.e(javaClass.simpleName, e.localizedMessage)
        }
    }

    private fun getDescription(faceInfoVO: FaceInfoVO, celebrityInfoVO: CelebrityInfoVO): String {
        val returnText = StringBuilder(celebrityInfoVO.celebrity!!.value)

        var celebritySimilar = ""
        when (celebrityInfoVO.celebrity.confidence) {
            in 0f..0.3f -> celebritySimilar = " 손톱 때만큼 닮은 "
            in 0.3f..0.5f -> celebritySimilar = " 뭉개놓은 것처럼 닮은 "
            in 0.5f..0.8f -> celebritySimilar = " 사아알~짝 닮은 "
            in 0.8f..1f -> celebritySimilar = " 인지 아닌지 헷갈릴 정도로 닮은 "
        }
        returnText.append(celebritySimilar)
        returnText.append("\n")

        val age: Int
        val ageSimilar: String
        val ages = faceInfoVO.age!!.value!!.split("~")
        if (ages.size > 1) {
            age = Integer.parseInt(ages[1].substring(0, 1))
            ageSimilar = ages[1].substring(0, 1)
        } else {
            age = Integer.parseInt(ages[0].substring(0, 1))
            ageSimilar = ages[0].substring(0, 1)
        }
        returnText.append(ageSimilar)
        returnText.append("0대 ")

        if ("male" == faceInfoVO.gender!!.value) {
            when (age) {
                1 -> returnText.append("남자 아이")
                in 4 .. 10 -> returnText.append("아저씨")
                else -> returnText.append("보통 청년")
            }
        } else {
            when (age) {
                1 -> returnText.append("귀요미 여자아이")
                in 4 .. 10 -> returnText.append("아줌마")
                else -> returnText.append("아가씨")
            }
        }

        return returnText.toString()
    }

    override fun onCancel() {
        view()?.hideProgress()
        view()?.setNotMatch()
    }

    fun getShareImage(file: File?) {
        kakaoInteractor.shareImage(file, this)
    }

    override fun onFailure(errorResult: ErrorResult) {
        Log.e(javaClass.simpleName, errorResult.errorMessage)
        view()?.showMessage(errorResult.errorMessage)
    }

    override fun onSuccess(result: KakaoLinkResponse) {
        view()?.hideProgress()
    }
}