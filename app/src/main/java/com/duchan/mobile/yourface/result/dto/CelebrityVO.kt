package com.duchan.mobile.yourface.result.dto

/**
 * Created by Busy on 2018. 9. 18..
 */
class CelebrityVO {
    val info: InfoVO? = null
    val faces: List<CelebrityInfoVO>? = null
}