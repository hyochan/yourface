package com.duchan.mobile.yourface.common

import android.os.Bundle
import com.duchan.mobile.mvp_sample.common.mvp.BaseMvpActivity
import com.duchan.mobile.yourface.common.mvp.MvpPresenter
import com.duchan.mobile.yourface.common.mvp.MvpView

abstract class CommonActivity<in V : MvpView, out P : MvpPresenter<V>> : BaseMvpActivity<V, P>() {


    protected abstract fun initLayoutView(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(initLayoutView())
    }

    override fun attachView(view: V) {
        // Hooking method
    }

    override fun detachView() {
        // Hooking method
    }
}