package com.duchan.mobile.yourface.common.uploader;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class UploaderQueue {
	private static final int KEEP_ALIVE_TIME_IN_SECONDS = 1;
	private static final int THREAD_POOL_SIZE = Runtime.getRuntime().availableProcessors();

	private Map<String, Uploader> taskMap = new ConcurrentHashMap<>();
	private ThreadPoolExecutor executor;

	private static UploaderQueue instance = new UploaderQueue();

	public static UploaderQueue getInstance() {
		return instance;
	}

	private UploaderQueue() {
		BlockingQueue<Runnable> taskQueue = new LinkedBlockingQueue<>();
		executor = new ThreadPoolExecutor(
				0,
				THREAD_POOL_SIZE,
				KEEP_ALIVE_TIME_IN_SECONDS,
				TimeUnit.SECONDS,
				taskQueue);
	}

	public void enqueue(Uploader uploader) {
		taskMap.put(uploader.getKey(), uploader);
		executor.execute(uploader);
	}

	public void release() {
		cancelAll();
		executor.shutdown();
	}

	public synchronized void cancel(String key) {
		Uploader uploader = taskMap.get(key);
		if (uploader != null) {
			uploader.cancel();
		}
	}

	public synchronized void cancelAll() {
		if (taskMap.isEmpty()) {
			return;
		}

		for (String key : taskMap.keySet()) {
			cancel(key);
		}
	}

	public synchronized void finish(String key) {
		taskMap.remove(key);
	}
}
