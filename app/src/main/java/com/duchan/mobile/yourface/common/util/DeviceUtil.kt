package com.duchan.mobile.yourface.common.util

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Point
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.view.WindowManager
import com.duchan.mobile.yourface.main.util.REQUEST_STORAGE_PERMISSION
import com.duchan.mobile.yourface.main.util.insertImage
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.thread


/**
 * Created by Busy on 2018. 9. 22..
 */
object DeviceUtil {
    interface deviceCallback {
        fun success(file: File?)
        fun error(error: String)
    }

    fun getDeviceSize(context: Context): IntArray {
        val size = IntArray(2)

        val measuredWidth: Int
        val measuredHeight: Int
        val point = Point()
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager

        wm.defaultDisplay.getSize(point)
        measuredWidth = point.x
        measuredHeight = point.y

        if (measuredHeight > measuredWidth) {
            size[0] = measuredWidth
            size[1] = measuredHeight
        } else {
            size[0] = measuredHeight
            size[1] = measuredWidth
        }

        return size
    }

    fun getDeviceWidth(context: Context): Int {
        val size = getDeviceSize(context)
        return size[0]
    }

    fun getDeviceHeight(context: Context): Int {
        val size = getDeviceSize(context)
        return size[1]
    }

    fun captureImage(view: View, fileName: String, callback: deviceCallback) {
        if (ContextCompat.checkSelfPermission(view.context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(view.context as Activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_STORAGE_PERMISSION)
        } else {
            val handler = Handler(Looper.getMainLooper())

            thread(start = true) {
                view.buildDrawingCache(true)

                val captureView = view.drawingCache
                var captureFile: File? = null
                try {
                    val mediaStorageDir = File(
                            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                            "YourFace")
                    // Create the storage directory if it does not exist
                    if (!mediaStorageDir.exists()) {
                        if (!mediaStorageDir.mkdirs()) {
                            handler.post { callback.error("Unable to create directory.") }
                        }
                    }
                    var newFileName = fileName
                    if (newFileName.isEmpty()) {
                        newFileName = SimpleDateFormat("yyyyMMdd_HHmmss",
                                Locale.getDefault()).format(Date()) + ".jpg"
                    }

                    captureFile = File(insertImage(view.context.contentResolver, captureView, newFileName , ""))
                } catch (e: IOException) {
                    Log.e(javaClass.simpleName, e.localizedMessage)
                    handler.post { callback.error(e.localizedMessage) }
                } finally {
                    handler.post { callback.success(captureFile) }
                }
            }
        }
    }
}